import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { timeStamp } from 'console';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto) {
    const product: Product = new Product();
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.productsRepository.save(product);
  }

  findAll() {
    return this.productsRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} product`;
  }

  async update(id: number, UpdateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneBy({ id: id });
    const updatedProduct = { ...product, ...UpdateProductDto };
    return this.productsRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const user = await this.productsRepository.findBy({ id: id });
    return this.productsRepository.remove(user);
  }
}
